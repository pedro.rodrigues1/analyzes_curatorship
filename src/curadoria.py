import pandas as pd
import os
from datetime import datetime
from sys import argv

now = datetime.now()
dt_string = now.strftime("%d-%m-%Y_%H:%M:%S")
# captura de data_hora


def tratamento(escolha: int) -> str:
    """Escolhe qual tipo de planilhas a serem trabalhadas

    Arguments:
        escolha {int} -- escolha um número inteiro de um 1 a 8

    Returns:
        str -- retorna o tipo de planilha 
    """
    if escolha == 1:
        return 'embargos'
    elif escolha == 2:
        return 'hearing'
    elif escolha == 3:
        return 'lock'
    elif escolha == 4:
        return 'phase'
    elif escolha == 5:
        return 'expert_proof'
    elif escolha == 6:
        return 'sentence'
    elif escolha == 7:
        return 'injunction'
    elif escolha == 8:
        return 'status'
    else:
        return 'Planilha desconhecida'


def return_path_rodada_anterior(escolha: str) -> str:
    """funcao que retorna o path da planilha da rodada anterior

    Arguments:
        escolha {str} -- recebe o retorno da funcao tratamento

    Returns:
        str -- path da planilha da rodada anterior
    """
    for root, dirs, files in os.walk(f"../rodada_anterior/{escolha}/"):
        print(f"Capturando path: {files}")
        return f"{root}{', '.join(files)}"


def return_path_ultima_rodada(escolha: str) -> str:
    """funcao que retorna o path da planilha da ultima_rodada

    Arguments:
        escolha {str} -- recebe o retorno da funcao tratamento

    Returns:
        str -- path da planilha da ultima_rodada
    """
    for root, dirs, files in os.walk(f"../ultima_rodada/{escolha}/"):
        print(f"Capturando path: {files}")
        return f"{root}{', '.join(files)}"


def return_dfs(path_rodada_anterior: str, path_ultima_rodada: str, escolha: str) -> pd.DataFrame:
    """ lê os xlsx e os retorna em DataFrames

    Arguments:
        path_rodada_anterior {str} -- recebe o retorno da funcao return_path_rodata_anterior
        path_ultima_rodada {str} -- recebe o retorno da funcao return_path_ultima_rodada
        escolha {str} -- recebe o retorno da funcao tratamento

    Returns:
        pd.DataFrame -- retorna dois DataFrames
    """
    df_rodada_anterior = pd.read_excel(path_rodada_anterior, usecols=[
                                       'numero_processo', escolha, 'mov_content', 'mov_date', 'curadoria'])
    df_ultima_rodada = pd.read_excel(path_ultima_rodada, usecols=[
                                     'numero_processo', escolha, 'mov_content', 'mov_date'])
    return df_rodada_anterior, df_ultima_rodada


def merge_dfs(df_ultima_rodada: pd.DataFrame, df_rodada_anterior: pd.DataFrame, escolha: str) -> pd.DataFrame:
    """ funcao que realiza o merge entre os dfs

    Arguments:
        df_ultima_rodada {pd.DataFrame} -- recebe o df_ultima_rodada da funcao return_dfs[1]
        df_rodada_anterior {pd.DataFrame} -- recebe o df_rodada_anterior da funcao return_dfs[0]
        escolha {str} -- recebe o retorno da funcao tratamento

    Returns:
        pd.DataFrame -- retorna um DataFrame como resultado do merge 
    """
    df_merge = pd.merge(df_ultima_rodada, df_rodada_anterior[['numero_processo', escolha, 'curadoria']], how='left', on=[
                        'numero_processo', 'numero_processo'], suffixes=('_ultima_rodada', '_rodada_anterior'))
    print('Fazendo merge')
    return df_merge


def calculo_da_alteracao(df: pd.DataFrame, escolha: str) -> pd.DataFrame:
    """ funcao que calcula de a regra foi alterada ou nao

    Arguments:
        df {pd.DataFrame} -- recebe o df_merge
        escolha {str} -- recebe o retorno da funcao tratamento

    Returns:
        pd.DataFrame -- retorna o dataFrame com a coluna alteracao
    """
    df['alteracao'] = df[[f'{escolha}_ultima_rodada', f'{escolha}_rodada_anterior']].apply(
        lambda row: 'Sem Alteração' if row[0] == row[1] else 'Alterado', axis=1)
    return df


def status_da_regra(row):
    """[summary]

    Arguments:
        row {[type]} -- Recebe a linha do df_merge
    Returns:
        [type] -- Retorna a resposta para a coluna de acordo com a regra aplicada
    """
    if row.alteracao == 'Sem Alteração' and row.curadoria == 'certo':
        # se não teve alteração e a curadoria aprovou...
        return 'Regra Válida'
    elif row.alteracao == 'Sem Alteração' and row.curadoria == 'errado':
        # se não teve alteração e a curadoria reprovou...
        return 'Revalidar regra'
    elif row.alteracao == 'Alteração' and row.curadoria == 'errado':
        # se teve alteracao e a curadoria reprovou...
        return 'Fazer nova curadoria'
    elif row.alteracao == 'Alteração' and row.curadoria == 'certo':
        # se teve alteracao e a curadoria aprovou
        return 'Regra quebrada'
    else:
        return 'Realizar curadoria'


def run_status_da_regra(df_merge):
    print('Calculando status da regra')
    df_merge['status_da_regra'] = df_merge.apply(status_da_regra, axis=1)


def calculate_area(row):
    return 'Trabalhista' if row.numero_processo[16] == '5' else 'Cível'


def run_calculate_area(df_merge):
    print('Calculando área')
    df_merge['area'] = df_merge.apply(calculate_area, axis=1)


def return_trabalhista_civel(df_merge, escolha):
    print('Separando processos')
    df_merge = df_merge[['numero_processo', escolha+'_rodada_anterior', 'curadoria', escolha +
                         '_ultima_rodada', 'alteracao', 'status_da_regra', 'mov_content', 'mov_date', 'area']]
    df_trabalhista = df_merge[df_merge['area'] == 'Trabalhista']
    df_civel = df_merge[df_merge['area'] == 'Cível']
    return df_trabalhista, df_civel


def calcula_cnjs(df_rodada_anterior, df_ultima_rodada):
    print('Calculando cnjs')
    cnj_ant_nc_ultima = df_rodada_anterior[~df_rodada_anterior['numero_processo'].isin(
        df_ultima_rodada['numero_processo'])]
    cnj_ult_nc_anterior = df_ultima_rodada[~df_ultima_rodada['numero_processo'].isin(
        df_rodada_anterior['numero_processo'])]
    cnj_ant_nc_ultima = cnj_ant_nc_ultima.rename(
        {'numero_processo': 'cnj_rodada_anterior_nao_consta_na_ultima_rodada'}, axis='columns').reset_index()
    cnj_ult_nc_anterior = cnj_ult_nc_anterior.rename(
        {'numero_processo': 'cnj_ultima_rodada_nao_consta_na_anterior'}, axis='columns').reset_index()
    cnj_ult_nc_anterior['cnj_rodada_anterior_nao_consta_na_ultima_rodada'] = cnj_ant_nc_ultima['cnj_rodada_anterior_nao_consta_na_ultima_rodada']
    cnj_final = cnj_ult_nc_anterior[[
        'cnj_ultima_rodada_nao_consta_na_anterior', 'cnj_rodada_anterior_nao_consta_na_ultima_rodada']]
    return cnj_final


def writer_excel(df_trabalhista, df_civel, df_cnj, escolha):
    print('Salvando arquivo')
    writer = pd.ExcelWriter(
        f"../resultados/resultado_{escolha}_{dt_string}.xlsx", engine='xlsxwriter')
    df_trabalhista.to_excel(writer, sheet_name='Trabalhista', index=False)
    df_civel.to_excel(writer, sheet_name='Cível', index=False)
    df_cnj.to_excel(writer, sheet_name='de_para_cnjs', index=False)
    writer.save()
    print(
        f"Arquivo salvo em: ../resultados/resultado_{escolha}_{dt_string}.xlsx")
    print('_'*70)


def main_curadoria(opcao):
    escolha = tratamento(opcao)
    print(f"Tratamento para coluna {escolha.upper()}")
    path_rodada_anterior = return_path_rodada_anterior(escolha)
    path_ultima_rodada = return_path_ultima_rodada(escolha)
    dfs = return_dfs(path_rodada_anterior, path_ultima_rodada, escolha)
    df_rodada_anterior = dfs[0]
    df_ultima_rodada = dfs[1]
    df_merge = merge_dfs(df_ultima_rodada, df_rodada_anterior, escolha)
    calculo_da_alteracao(df_merge, escolha)
    run_status_da_regra(df_merge)
    run_calculate_area(df_merge)
    df_trabalhista = return_trabalhista_civel(df_merge, escolha)[0]
    df_civel = return_trabalhista_civel(df_merge, escolha)[1]
    df_cnj = calcula_cnjs(df_rodada_anterior, df_ultima_rodada)
    writer_excel(df_trabalhista, df_civel, df_cnj, escolha)


if __name__ == "__main__":
    opcao = int(argv[1])
    main_curadoria(opcao)
