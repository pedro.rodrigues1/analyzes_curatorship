# analyzes_curatorship

 - Script que lê 2 arquivos em xlsx, separados por rodada anterior e ultima rodada, ambos seguem um mesmo padrão de colunas, exceto por uma das colunas variarem de acordo com sua regra e por um dos arquivos possuir a coluna curadoria preenchida. 
 - O script faz um merge entre as colunas dos arquivos com base no número de processo, trazendo a curadoria, mov_content(ultima_rodada), mov_date(ultima_rodada), cria uma coluna de status da regra, separa os processos por área, e calcula os cnjs que se diferem nas planilhas.

#### As planilhas devem estar dentro de seus respectivos diretórios.
#### A estrutura dos arquivos se baseiam em :
    -src
        -curadoria.py
        -play_all.sh

    -rodada_anterior
        -embargos
        -expert_proof
        -hearing
        -injuction
        -lock
        -phase
        -sentence
        -status

    -ultima_rodada
        -embargos
        -expert_proof
        -hearing
        -injuction
        -lock
        -phase
        -sentence
        -status

    - resultados

### Como rodar o script para calcular planilhas específicas:

- Entrar no diretorio scr
- Abrir o terminal
- Executar o comando: python3 curadoria.py parametro
- Exemplo: python3 curadoria.py 2
- Lista de parametros de 1 a 8:
- 1 = 'embargos'
- 2 = 'hearing'
- 3 = 'lock'
- 4 = 'phase'
- 5 = 'expert_proof'
- 6 = 'sentence'
- 7 = 'injunction'
- 8 = 'status'


### Como rodar o script para calcular todas as planilhas:
#### - Rodar o arquivo play_all.sh

#### Os retornos serão salvos na pasta resultados, com o nome e data/hora de suas respectivas planilhas.


### Segue entendimento da planilha de resultados do de/para dos cnjs curados.

A planilha de resultados terá uma coluna "status_da_regra", esta pode possuir os seguintes valores:

`Regra Válida`: Se a curadoria aprovou e não teve alteração na regra.

`Revalidar regra`: Se a curadoria reprovou e não teve alteração na regra.

`Fazer nova curadoria`: Se a curadoria reprovou e teve alteração na regra.

`Regra quebrada`: Se a curadoria aprovou e teve alteração na regra.

`Realizar curadoria`: Caso não atenda nenhuma das condições acima.

